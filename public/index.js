var $ = require('jquery')
// Bootstrap wants jQuery global =(
window.jQuery = $
// var _ = require('lodash')

require('popper.js/dist/umd/popper')
require('bootstrap/dist/js/bootstrap')
// const convert = require('xml-js')

var cssify = require('cssify')
cssify.byUrl('vendor/bootstrap/dist/css/bootstrap.min.css')
cssify.byUrl('vendor/tablesorter/theme.default.min.css')
cssify.byUrl('style.css')

// var BGGxmlapi2 = require('./modules/xmlapi2')
var UserPlays = require('./modules/user_plays')
var UserData = require('./modules/user_data')
var UserCollection = require('./modules/user_collection')

const queryString = window.location.search
const urlParams = new URLSearchParams(queryString)
const user = urlParams.get('user')
const pass = urlParams.get('pass')

// var apiCall = BGGxmlapi2.getApiCalls(user)
UserData.bggName = user
UserData.pass = pass
UserPlays.entireUserPlays(user, 1)
UserCollection.getUserCollection(user, 1)
