var BGGxmlapi2 = require('./xmlapi2')
var UserData = require('./user_data')
var Encrypt = require('./encrypt')
var $ = require('jquery')
var convert = require('xml-js')
var _ = require('lodash')

var UserCollection = {
  getUserCollection: function (user, page) {
    var apiCall = BGGxmlapi2.getApiCalls(user)
    // Two calls are needed to get boardgames and boardgames-expansions
    // due to a bug in the API that incorrectly gives subtype=boardgame for the expansions when
    // using subtype=boardgame (see: https://boardgamegeek.com/wiki/page/BGG_XML_API2#toc11)
    $.when($.ajax(apiCall.expansions), $.ajax(apiCall.boardgames)).done(
      function (userExpArg, userGamesArg) {
        console.log('api calls [collection]:')
        console.log(apiCall.expansions)
        console.log(apiCall.boardgames)
        // Each argument is an array with the following structure: [ data, statusText, jqXHR ]
        var boardgameExpansionsXML = userExpArg[0].scrollingElement.outerHTML
        var boardgamesXML = userGamesArg[0].scrollingElement.outerHTML

        var result1 = convert.xml2json(boardgameExpansionsXML, {
          compact: true,
          spaces: 4,
        })
        var result2 = convert.xml2json(boardgamesXML, {
          compact: true,
          spaces: 4,
        })
        var bg = JSON.parse(result2)
        var bgExpansions = JSON.parse(result1)

        UserData.gamesOwned = _.toNumber(bg.items._attributes.totalitems)
        UserData.expansionsOwned = _.toNumber(
          bgExpansions.items._attributes.totalitems
        )
        UserData.boardgames = bg.items.item

        // get encrypt price in public comments:
        var boardgames = UserData.boardgames
        boardgames.forEach(function (play, index, arr) {
          if (bg.items.item[index].comment) {
            var bggComment = bg.items.item[index].comment._text

            // get  '[' + codedPrice + ']'
            var codedPrice = bggComment.substring(bggComment.indexOf('['))
            codedPrice = codedPrice.substring(0, codedPrice.indexOf(']') + 1)
            if (codedPrice) {
              var passcode = UserData.pass
              // encrypt:
              // var content = '40'
              // var encodingText = Encrypt.encryptCodes(content, passcode)
              // console.log('****************encodingText ', content, encodingText)
              // end encryption script
              var pricePaid = _.toNumber(
                Encrypt.decryptCodes(codedPrice, passcode)
              )

              UserData.boardgames[index].pricePaid = pricePaid.toFixed(2)
            }
          }
        })

        // end testing
        UserData.expansions = bgExpansions.items.item

        $('#gamesOwned').text(UserData.gamesOwned + ' games owned')
        $('#expansionsOwned').text(
          UserData.expansionsOwned + ' games expansions owned'
        )
      }
    )
  },
}

module.exports = UserCollection
