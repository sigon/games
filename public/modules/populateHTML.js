var UserData = require('./user_data')
var PlayCharts = require('./playcharts')
var $ = require('jquery')
var tablesorter = require('tablesorter')
var ProgressBar = require('progressbar.js')

var Populate = {
  profile: function () {
    var avatarLInk = UserData.profile.avatarLink
    $('#bggAvatar').attr('src', avatarLInk)
  },
  totalPlays: function () {
    var bggUrl = 'https://www.boardgamegeek.com/user/'
    var bggProfileLink = bggUrl + UserData.bggName
    $('#bggUsername').attr('href', bggProfileLink)
    $('#bggUsername').text('BGG profile / ' + UserData.bggName)
    $('#totalPlays').text(UserData.plays._attributes.boardgame + ' total plays')
  },
  playsByYear: function () {
    var playsArray = UserData.plays.playsByYear
    $.each(playsArray, function (i, plays) {
      $('#tablePlaysByYear').append('<tr><td>' + plays.year + ': ' + plays.total + '</td></tr>')
    })
  },
  thirtyDaysPlays: function () {
    var thirtyDays = UserData.plays.thirtyDays
    $('#monthPlays').text(thirtyDays.total + ' total plays')
  },
  challenges: function (challengeIndex) {
    $('#challenges').append('<h3>Challenges</h3>')
    var challenges = UserData.challenges[challengeIndex]
    $.each(challenges, function (i, challenge) {
      if (challenge.totalPlays) {
        var total = challenge.totalPlays
        $('#challenges').append('<tr><td>' + total.text + ', ' + total.progress.left + ' plays left</td></tr>')
        Populate.getProgressBar(total.progress.percentage, 'totalPlaysBar')
      }
      if (challenge.play10x10) {
        // todo
      }
    })
  },
  getProgressBar: function (percentage) {
    var progress = (percentage > 100) ? 100 : percentage
    var progressBarHTML = '<div class="progress">' +
      '<div class="progress-bar" role="progressbar" aria-valuenow="' + progress + '" aria-valuemin="0"' +
      'aria-valuemax="100" style="width: ' + progress + '%;">' +
      '<span class="sr-only">' + progress + '% Complete</span>' + progress + '%' +
      '</div>' +
      '</div>'
    $('#challenges').append(progressBarHTML)
  },
  hIndex: function () {
    $('#hIndex').text(UserData.hIndex)
  },
  tableGames: function () {
    var gamesPlayedArray = UserData.gamesPlayed
    var position = 1
    var bggLinkRoot = 'https://www.boardgamegeek.com/boardgame/'

    $.each(gamesPlayedArray, function (i, game) {
      var gameLink = bggLinkRoot + game.gameID
      if (game.type === 'boardgame' && game.owner === 'yes') {
        var gameText = game.name
        var totalPlays = game.total + ' plays'
        var gameThumbnail = game.thumbnail || '/favicon.ico'
        var imgLInk = '<div class="imgdiv">' + '<a href="' + gameLink + '"><img src="' +
          gameThumbnail + '" alt="' + game.name +
          '" class="img-thumbnail"></a></div>'
        var text = position + '</th><td>' + imgLInk + '</td><td class="gameName">' + gameText +
          '</td><td class="totalplays" style="cursor:pointer" title="See plays in chart">' + totalPlays +
          '</td><td>' + game.pricePaid

        // total plays length (minutes to hours and minutes)
        var hours = Math.floor(game.lenght / 60)
        var minutes = game.lenght % 60
        var totalLenght = '</td><td>' + hours + '.' + minutes + ' h'

        var lastPlayed = '</td><td>' + game.lastPlayed
        var lastPlayedDate = '</td><td>' + game.lastPlayedDate
        var rowText = '<tr><th scope="row">' + text + totalLenght + lastPlayed + lastPlayedDate + '</td></tr>'
        $('#tableGames').append(rowText)
      }
      position++
    })
    // click event on game plays number: scroll to chart and create chart
    $(function () {
      $('.totalplays').click(function () {
        console.log('click')
        $('html, body').animate({
          scrollTop: $('#chartContainer').offset().top
        }, 1000)

        var gameName = $(this)[0].previousSibling.textContent
        PlayCharts.getGameSeries(gameName)
      })
    })
    // tablesorter plugin (turning into sortable table)
    $(function () {
      $('#tableGames').tablesorter({
        theme: 'default'
      })
    })
  }
}

module.exports = Populate
