var BGGxmlapi2 = require('./xmlapi2')
var UserData = require('./user_data')
var ChallengesList = require('./challenges/challenges_list')
var UserChallenges = require('./challenges/user_challenges')

var GamesPlayed = require('./games_played')
var Populate = require('./populateHTML')
var moment = require('moment')
var $ = require('jquery')
var convert = require('xml-js')
var _ = require('lodash')

var UserPlays = {
  entireUserPlays: function (user, page) {
    var apiCall = BGGxmlapi2.getApiCalls(user)
    // console.log('api calls [plays]:')
    // console.log(apiCall.playsTotal + '&page=' + page)

    $.when(
      $.ajax(apiCall.playsTotal + '&page=' + page),
      $.ajax(apiCall.userProfile)
    ).done(function (plays, profileData) {
      console.log('api calls [plays]:')
      console.log(apiCall.playsTotal + '&page=' + page)
      var playsXML = plays[0].scrollingElement.outerHTML
      var result1 = convert.xml2json(playsXML, { compact: true, spaces: 4 })
      var bgPlays = JSON.parse(result1)

      // user total plays on bgg:
      UserData.plays._attributes = bgPlays.plays._attributes
      UserData.plays.play = bgPlays.plays.play

      // check if more pages are needed
      var totalPlaysNumber = _.toNumber(UserData.plays._attributes.total)
      const totalApiPages = _.ceil(totalPlaysNumber / 100)
      if (totalApiPages > 2) {
        getMorePages(2, totalApiPages)
      }

      // user profile data:
      var profileXML = profileData[0].scrollingElement.outerHTML
      var result2 = convert.xml2json(profileXML, { compact: true, spaces: 4 })
      var profile = JSON.parse(result2)
      UserData.profile.avatarLink = profile.user.avatarlink._attributes.value
      UserData.profile.yearRegisred =
        profile.user.yearregistered._attributes.value
    })
  },
}
module.exports = UserPlays

// new api call if more pages needed
function getMorePages(page, totalApiPages) {
  var apiCall = BGGxmlapi2.getApiCalls(UserData.bggName)
  if (page > totalApiPages) {
    prepareUserPlaysData()
    return
  }
  $.ajax({
    url: apiCall.playsTotal + '&page=' + page,
    type: 'GET',
    data: 'xml',
    datatype: 'xml',
  })
    .done(function (data) {
      successFunction(data)
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      errorFunction(textStatus)
    })

  function successFunction(data) {
    console.log(apiCall.playsTotal + '&page=' + page)
    var playsXML = data.scrollingElement.outerHTML
    var result = convert.xml2json(playsXML, { compact: true, spaces: 4 })
    var bgPlays = JSON.parse(result)
    var totalPagesPlaysCurrentArray = UserData.plays.play
    var totalPagesPlays = _.union(
      totalPagesPlaysCurrentArray,
      bgPlays.plays.play
    )
    UserData.plays.play = totalPagesPlays
    page++
    getMorePages(page, totalApiPages)
  }
  function errorFunction(textStatus) {
    console.log(textStatus)
  }
}
// count all plays
function prepareUserPlaysData() {
  console.log(UserData)
  var userPlays = UserData.plays.play
  var userTotalPlays = _.toNumber(UserData.plays._attributes.total)
  var userTotalExpansionPlays = 0

  userPlays.forEach(function (play, index, arr) {
    // calculate user total plays number:
    var playQuantity = _.toNumber(play._attributes.quantity)
    if (playQuantity > 1) {
      userTotalPlays = userTotalPlays + playQuantity - 1
    }
    if (
      play.item.subtypes.subtype[1] &&
      play.item.subtypes.subtype[1]._attributes.value === 'boardgameexpansion'
    ) {
      userTotalExpansionPlays = playQuantity + userTotalExpansionPlays
    }
    // convert string-date to date format and aggregate data in years
    var playDate = new Date(play._attributes.date)
    UserData.plays.play[index]._attributes.date = playDate
    var userPlaysByYear = UserData.plays.playsByYear
    var year = playDate.getFullYear()
    var month = moment(playDate).month()
    var monthName = moment(playDate).format('MMMM')
    var playIndexYear = _.findIndex(userPlaysByYear, ['year', year])
    if (playIndexYear === -1) {
      userPlaysByYear.push({
        year: year,
        play: [play],
        total: playQuantity,
        byMonth: [
          {
            month: month,
            monthName: monthName,
            play: [play],
            total: playQuantity,
          },
        ],
      })
    } else {
      var yearTotal = userPlaysByYear[playIndexYear].total + playQuantity
      userPlaysByYear[playIndexYear].play.push(play)
      userPlaysByYear[playIndexYear].total = yearTotal
      // plays by months
      var playIndexMonth = _.findIndex(userPlaysByYear[playIndexYear].byMonth, [
        'month',
        month,
      ])
      if (playIndexMonth === -1) {
        userPlaysByYear[playIndexYear].byMonth.push({
          month: month,
          monthName: monthName,
          play: [play],
          total: playQuantity,
        })
      } else {
        var monthCount =
          userPlaysByYear[playIndexYear].byMonth[playIndexMonth].total
        userPlaysByYear[playIndexYear].byMonth[playIndexMonth].play.push(play)
        userPlaysByYear[playIndexYear].byMonth[playIndexMonth].total =
          monthCount + playQuantity
      }
    }
    UserData.plays.playsByYear = userPlaysByYear
    // get last 30 days plays
    var thirtyDaysPlays = UserData.plays.thirtyDays
    var thirtyDaysTotal = _.toNumber(thirtyDaysPlays.total)
    const endDate = moment()
    const startDate = moment().subtract(30, 'days')
    const date = moment(play._attributes.date)
    var isInRange = date.isBetween(startDate, endDate) // all inclusive
    if (isInRange) {
      thirtyDaysPlays.plays.push(play)
      thirtyDaysTotal = thirtyDaysTotal + playQuantity
    }
    thirtyDaysPlays.total = thirtyDaysTotal
    UserData.plays.thirtyDays = thirtyDaysPlays
  })
  GamesPlayed.getList()
  UserData.plays._attributes.boardgame = userTotalPlays
  UserData.plays._attributes.expansions = userTotalExpansionPlays
  Populate.totalPlays()
  Populate.playsByYear()
  Populate.thirtyDaysPlays()
  Populate.profile()

  if (UserData.bggName === 'sigon') {
    UserData.challenges = ChallengesList
    var challenges = UserData.challenges
    var currentYear = moment().year()
    var challengeIndex = _.findIndex(challenges, { year: currentYear })
    UserChallenges.getProgress(currentYear, challengeIndex)
  }
}
