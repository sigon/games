var UserData = require('./user_data')
var $ = require('jquery')
var Populate = require('./populateHTML')
var PlayCharts = require('./playcharts')
// var DateFormat = require('./date')
var _ = require('lodash')
var moment = require('moment')
var convert = require('xml-js')

var GamesPlayed = {
  getList: function () {
    var userPlays = UserData.plays.play
    userPlays.forEach(function (play, index, arr) {
      var gamesPlayed = UserData.gamesPlayed
      var name = play.item._attributes.name // game name
      var id = play.item._attributes.objectid // gameid
      var subtype = play.item.subtypes.subtype
      var type = '' // boardgame or extension
      if (Array.isArray(subtype)) {
        type = play.item.subtypes.subtype[0]._attributes.value
      } else {
        type = play.item.subtypes.subtype._attributes.value
      }
      var playLength = _.toNumber(play._attributes.length)
      var playQuantity = _.toNumber(play._attributes.quantity)
      var playIndex = _.findIndex(gamesPlayed, ['gameID', id])
      if (playIndex === -1) {
        gamesPlayed.push({ name: name, gameID: id, type: type, play: [play], total: playQuantity, lenght: playLength })
      } else {
        var gameTotal = gamesPlayed[playIndex].total + playQuantity
        var gameTotalLenght = gamesPlayed[playIndex].lenght + playLength
        gamesPlayed[playIndex].play.push(play)
        gamesPlayed[playIndex].total = gameTotal
        gamesPlayed[playIndex].lenght = gameTotalLenght
      }
      UserData.gamesPlayed = _.orderBy(gamesPlayed, ['total'], ['desc'])
    })
    this.getHindex()
  },
  getHindex: function () {
    // Your h-index is the smallest number of games that you have played at least that number of times.
    // with the game played array is ordered desc by 'total' plays
    // then look for the last position in which 'total' value is greater than or equal
    // to the position (h this position)
    var gamesPlayed = UserData.gamesPlayed
    var hIndex = 0
    var hIndexArray = []
    gamesPlayed.forEach(function (game, index, arr) {
      var total = game.total // number of plays of this game
      if (index + 1 <= total) {
        hIndexArray.push(game)
        hIndex = index + 1
      }
    })
    UserData.hIndex = hIndex
    this.completeList()
    Populate.hIndex()
  },
  completeList: function () {
    var boardgames = UserData.boardgames
    var expansions = UserData.expansions
    var gamesPlayed = UserData.gamesPlayed
    var gamesPlayedIDs = [] // gameid array
    gamesPlayed.forEach(function (game, index, arr) {
      var id = game.gameID
      var boardgamesIndex = _.findIndex(boardgames, ['_attributes.objectid', id])
      var expansionsIndex = _.findIndex(expansions, ['_attributes.objectid', id])
      gamesPlayedIDs.push(_.toNumber(gamesPlayed[index].gameID))
      if (boardgamesIndex === -1 & expansionsIndex === -1) {
        // games played but not in user collection
        gamesPlayed[index].pricePaid = 0
        gamesPlayed[index].owner = 'no'

        // gamesPlayedNotInCollection.push(_.toNumber(gamesPlayed[index].gameID))
      } else if (boardgamesIndex === -1) {
        // is an expansion
        gamesPlayed[index].thumbnail = expansions[expansionsIndex].thumbnail._text
        gamesPlayed[index].type = expansions[expansionsIndex]._attributes.subtype
      } else {
        gamesPlayed[index].thumbnail = boardgames[boardgamesIndex].thumbnail._text
        gamesPlayed[index].pricePaid = (boardgames[boardgamesIndex].pricePaid) ? boardgames[boardgamesIndex].pricePaid : 0
        gamesPlayed[index].owner = 'yes'
      }
      // last played
      var lastPlayedDate = game.play[0]._attributes.date
      gamesPlayed[index].lastPlayed = moment(lastPlayedDate).fromNow()
      gamesPlayed[index].lastPlayedDate = moment(lastPlayedDate).format('DD-MM-YYYY')
    })
    UserData.gamesPlayed = gamesPlayed
    UserData.gamesPlayedIds = gamesPlayedIDs
    this.getGamesInfo(gamesPlayedIDs)
    this.getChartSeries()
  },
  getChartSeries: function () {
    var playsByYear = UserData.plays.playsByYear
    var dataseries = []
    var xAxis = { categories: [] }
    playsByYear.forEach(function (plays, index, arr) {
      xAxis.categories.push(plays.year)
      dataseries.push({ name: plays.year, y: plays.total })
    })
    var dataseriesReverse = _.reverse(dataseries)
    var seriesArray = [{
      name: 'plays',
      data: dataseriesReverse
    }]
    this.getGamePlays()

    var xaxisCategoriesReverse = _.reverse(xAxis.categories)
    xAxis.categories = xaxisCategoriesReverse

    UserData.yearCharts[0] = seriesArray
    UserData.yearCharts[1] = xAxis
    PlayCharts.createYearsChart(seriesArray, xAxis)
  },
  getGamePlays: function () {
    UserData.gamesPlayed.forEach(function (game, gameIndex, arr) {
      var playSeries = []
      game.play.forEach(function (play, playIndex, arr) {
        var month = moment(play._attributes.date).month()
        var year = moment(play._attributes.date).year()
        var playSeriesIndex = _.findIndex(playSeries, { year: year, month: month })
        var playQuantity = _.toNumber(play._attributes.quantity)
        if (playSeriesIndex === -1) {
          playSeries.push({ year: year, month: month, total: playQuantity })
        } else {
          var playQuantityOld = playSeries[playSeriesIndex].total
          playSeries[playSeriesIndex].total = playQuantity + playQuantityOld
        }
      })
      UserData.gamesPlayed[gameIndex].playSeries = playSeries
    })
  },
  getGamesInfo: function (idsArray) {
    // call to get games information. comma-delimited list of game ids.
    var apiroot = 'https://www.boardgamegeek.com/xmlapi2/'
    var apiCallforgames = apiroot + 'thing?id=' + idsArray.toString()

    $.when($.ajax(apiCallforgames)).done(function (response) {
      console.log(apiCallforgames)
      var playsXML = response.scrollingElement.outerHTML
      var result1 = convert.xml2json(playsXML, { compact: true, spaces: 4 })
      var games = JSON.parse(result1)
      UserData.gamesPlayedCollection = games.items.item

      // complete UserData.gamesPlayed with the thumbnail
      var gamesPlayed = UserData.gamesPlayed
      gamesPlayed.forEach(function (game, index, arr) {
        if (!game.thumbnail) {
          UserData.gamesPlayed[index].thumbnail = games.items.item[index].thumbnail._text
        }
      })
      Populate.tableGames()
    })
  }
}

module.exports = GamesPlayed
