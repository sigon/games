var UserData = require('./user_data')
var Highcharts = require('highcharts')
var moment = require('moment')
var _ = require('lodash')
// Load module after Highcharts is loaded
require('highcharts/modules/exporting')(Highcharts)

// Create the chart
var PlayCharts = {
  createYearsChart: function (seriesArray, xAxis) {
    Highcharts.chart('chartContainer', {
      chart: {
        type: 'column'
      },
      series: seriesArray,
      title: {
        text: 'total plays by year'
      },
      xAxis: xAxis,
      yAxis: {
        title: {
          text: 'number of plays'
        }
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function () {
                PlayCharts.getChartSeries(this.name)
              }
            }
          }
        }
      }
    })
  },
  // create chart with month plays when clicked specfic year in year's play chart
  createMonthsChart: function (year, seriesArray, xAxis) {
    Highcharts.chart('chartContainer', {
      chart: {
        type: 'column'
      },
      series: seriesArray,
      title: {
        text: 'year ' + year + ': total plays by month'
      },
      xAxis: xAxis,
      yAxis: {
        title: {
          text: 'number of plays'
        }
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function () {
                // alert('Category: ' + this.category + ', value: ' + this.y + 'index: ' + this.index)
                // this.getChartSeries(this.index)
              }
            }
          }
        }
      }
    })
  },
  // create chart: number of plays by month for selected game
  createGamePlaysChartA: function (data, gameName, xAxis) {
    Highcharts.chart('chartContainer', {
      chart: {
        zoomType: 'x',
        type: 'column',
        events: {
          load: function () {
            this.renderer.button('back', 5, 5)
              .on('click', function () {
                var seriesArray = UserData.yearCharts[0]
                var xAxis = UserData.yearCharts[1]
                PlayCharts.createYearsChart(seriesArray, xAxis)
              })
              .add()
          }
        }
      },
      title: {
        text: gameName + ' (total plays) '
      },
      subtitle: {
        text: document.ontouchstart === undefined
          ? 'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
      },
      xAxis: xAxis,
      yAxis: {
        title: {
          text: 'num plays'
        }
      },
      legend: {
        enabled: false
      },
      series: [{
        name: 'Num Plays',
        data: data
      }]
    })
  },
  // create data series: number of plays by year
  getChartSeries: function (yearname) {
    var yearIndex = _.findIndex(UserData.plays.playsByYear, { year: yearname })
    var playsByYear = UserData.plays.playsByYear[yearIndex]
    var playsByMonth = playsByYear.byMonth
    var dataseries = []
    var xAxis = { categories: [] }
    playsByMonth.forEach(function (plays, index, arr) {
      xAxis.categories.push(plays.monthName)
      dataseries.push({ name: plays.monthName, y: plays.total })
    })
    var dataseriesReverse = _.reverse(dataseries)
    var xaxisCategoriesReverse = _.reverse(xAxis.categories)
    xAxis.categories = xaxisCategoriesReverse

    var seriesArray = [{
      name: 'plays',
      data: dataseriesReverse
    }]
    var year = UserData.plays.playsByYear[yearIndex].year
    this.createMonthsChart(year, seriesArray, xAxis)
  },
  // create data series: number of plays by month for selected game
  getGameSeries: function (gameName) {
    var data = []
    var gameIndex = _.findIndex(UserData.gamesPlayed, { name: gameName })
    var gamePlays = UserData.gamesPlayed[gameIndex].playSeries
    var xAxis = { categories: [] }

    gamePlays.forEach(function (mPlays, index, arr) {
      var monthDate = moment([mPlays.year, mPlays.month])
      var dateText = monthDate.format('MMM´YY') // "Aug'19"
      xAxis.categories.push(dateText)
      data.push({ name: dateText, y: mPlays.total })
    })
    var dataseriesReverse = _.reverse(data)
    var xaxisCategoriesReverse = _.reverse(xAxis.categories)
    xAxis.categories = xaxisCategoriesReverse
    this.createGamePlaysChartA(dataseriesReverse, gameName, xAxis)
  }
}

module.exports = PlayCharts
