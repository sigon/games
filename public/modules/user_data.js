var UserData = {
  bggName: 'user',
  profile: {
    avatarLink: '',
    yearRegisred: 0
  },
  gamesOwned: 0, // number of games owned
  expansionsOwned: 0, // number of expansion owned
  boardgames: {}, // bgg games
  expansions: {}, // bgg game expansions
  plays: {
    _attributes: {},
    play: [], // array of plays
    playsByYear: [], // array of games played ordered by year
    thirtyDays: { // games played in the last 30 days
      plays: [], // array of plays
      total: 0
    }
  },
  yearCharts: [],
  gamesPlayed: [], // array of games played ordered by the most played to the less
  hIndex: 0, // number of games that you have played at least that number of times
  challenges: []
}

module.exports = UserData
