var ChallengesList = [
  {
    year: 2023,
    challenges: {
      totalPlays: {
        text: 'Play 365 times in a year',
        startDate: '01-01-2023',
        endDate: '31-12-2023',
        plays: 365
      }
    }
  },
  {
    year: 2022,
    challenges: {
      totalPlays: {
        text: 'Play 300 times in a year',
        startDate: '01-01-2022',
        endDate: '31-12-2022',
        plays: 300
      }
    }
  },
  {
    year: 2021,
    challenges: {
      totalPlays: {
        text: 'Play 300 times in 2021',
        startDate: '01-01-2021',
        endDate: '31-12-2021',
        plays: 300
      },
      play10x10: {
        text: 'Play 10 Games 10 times each',
        startDate: '01-05-2021',
        endDate: '31-12-2021',
        list: [
          {
            name: 'After The Virus', objectid: '232361'
          },
          {
            name: 'One Deck Dungeon', objectid: '179275'
          },
          {
            name: 'MOON', objectid: '276161'
          },
          {
            name: 'Señor de los Anillos', objectid: '823'
          }
        ]
      }
    }
  },
  {
    year: 2020,
    challenges: {
      totalPlays: {
        text: 'Play 200 times in a year',
        startDate: '01-01-2020',
        endDate: '31-12-2020',
        plays: 200
      }
    }
  }
]

module.exports = ChallengesList
