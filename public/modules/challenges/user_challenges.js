var UserData = require('../user_data')
var ChallengesList = require('./challenges_list')
var Populate = require('../populateHTML')
var _ = require('lodash')

var UserChallenges = {
  list: ChallengesList,
  progress: [],
  getProgress: function (currentYear, challegeIndex) {
    var cChallengesPlaysIndex = _.findIndex(UserData.plays.playsByYear, { year: currentYear })
    var cChallengesPlays = UserData.plays.playsByYear[cChallengesPlaysIndex]
    var cChallengesList = this.list[challegeIndex]

    this.totalPlays(cChallengesPlays.total, cChallengesList.challenges.totalPlays.plays, challegeIndex)
    this.play10x10()
  },
  totalPlays: function (current, total, challengeIndex) {
    var percentage = Math.floor((current / total) * 100)
    var playsLeft = total - current
    playsLeft = (playsLeft < 0) ? 0 : playsLeft
    var progress = { percentage: percentage, current: current, left: playsLeft }
    UserData.challenges[challengeIndex].challenges.totalPlays.progress = progress
    Populate.challenges(challengeIndex)
  },
  play10x10: function () {
    // loop to plays
  }
}

module.exports = UserChallenges
