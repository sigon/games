// get bgg xmlapi2 path

var BGGdate = require('./date')

var xmlapi2 = {
  username: null,
  root: 'https://www.boardgamegeek.com/xmlapi2/',

  getApiCalls: function (user) {
    const collection = this.root + 'collection?username='
    const plays = this.root + 'plays?username='
    const userBoardgames = collection + user + '&excludesubtype=boardgameexpansion&showprivate=1'
    const userExpansions = collection + user + '&subtype=boardgameexpansion&showprivate=1'
    const userPlays = plays + user
    const userPlays30days = plays + user + '&' + BGGdate.last30days()
    const userPlayscYear = plays + user + '&' + BGGdate.currentYear()
    const userProfile = this.root + 'user?name=' + user

    const apiCalls = {
      userProfile: userProfile,
      boardgames: userBoardgames,
      expansions: userExpansions,
      playsTotal: userPlays,
      plays30days: userPlays30days,
      playsCurrentYear: userPlayscYear
    }
    return apiCalls
  }
}

module.exports = xmlapi2
