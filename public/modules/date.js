// https://www.boardgamegeek.com/xmlapi2/plays?username=USERNAME&mindate=YYYYMMDD&maxdate=YYYYMMDD
var moment = require('moment')

var GetDate = {
  currentYear: function () {
    const cYear = moment().year()
    const mindate = 'mindate=' + cYear + '0101'
    const maxdate = 'maxdate=' + cYear + '1231'
    return mindate + '&' + maxdate
  },
  last30days: function () {
    const today = moment().format('YYYYMMDD')
    const YYYYMMDD = moment().subtract(30, 'days').format('YYYYMMDD')
    const mindate = 'mindate=' + YYYYMMDD
    const maxdate = 'maxdate=' + today
    return mindate + '&' + maxdate
  },
  unixTimestamp: function (date) {
    var unixDate = moment(date).valueOf()
    return unixDate
  }
}

module.exports = GetDate
